# -*- coding: utf-8 -*-

import sys
import urllib
import urlparse
import xbmcgui
import xbmcplugin
import YDStreamExtractor
from nrl import NRL

# Plugin params
addon_name   =   'plugin.video.nrl'
base_url     = sys.argv[0]
addon_handle = int(sys.argv[1])

xbmc.log('%s: Started. _addon_url: %s Params: %s' % (addon_name, base_url, sys.argv[2]), xbmc.LOGNOTICE)

def build_url(query):
    return base_url + '?' + urllib.urlencode(query)

args = urlparse.parse_qs(sys.argv[2][1:])
mode = args.get('mode', None)

if mode is None:
    nrl = NRL()
    for event_list in nrl.event_lists.iteritems():
        url = build_url({'mode': 'folder', 'status': event_list[0]})
        foldername = "Games with status = %s" % event_list[0]
        li = xbmcgui.ListItem(foldername, iconImage='DefaultFolder.png')
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=url,
                                    listitem=li, isFolder=True)

elif mode[0] == 'folder':
    nrl = NRL()
    status = args['status'][0]
    events = nrl.event_lists[status]
    for event in events:
        aired = event.date_text if event.timestamp is None else str(event.timestamp)
        label = '{} ({})'.format(event.full_name, aired)
        url = build_url({'mode': 'event', 'href': event.href, 'aired': aired})
        li = xbmcgui.ListItem(label, iconImage = event.img_src)
        xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li, isFolder = True)

elif mode[0] == 'event':
    href = args['href'][0]
    aired = args['aired'][0]
    url = 'https://livestream.com' + href
    vid = YDStreamExtractor.getVideoInfo(url)
    if vid is None:
        raise Exception('YDStreamExtractor.getVideoInfo("{}") returned None'.format(url))
    stream_url = vid.streamURL()
    label = '{} ({})'.format(vid.title, aired)
    li = xbmcgui.ListItem(label, iconImage = vid.thumbnail)
    li.setInfo( type='Video', infoLabels={'title': vid.description, 'aired': aired } )
    xbmcplugin.addDirectoryItem(handle=addon_handle, url=stream_url, listitem=li)

xbmcplugin.endOfDirectory(addon_handle)

