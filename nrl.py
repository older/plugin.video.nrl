# -*- coding: utf-8 -*-

import json
import re
import urllib2

class Event:
    status_future = "future"
    status_past = "past"

    def __init__(self):
        self.full_name = None
        self.href = None
        self.status = None
        self.img_src = None
        self.date_text = None
        self.timestamp = None
    
    def __str__(self):
        return "Event: full_name=%s, href=%s, status=%s, img_src=%s, date_text=%s, timestamp=%s" % \
               (self.full_name, self.href, self.status, self.img_src, self.date_text, self.timestamp)

class NRL(object):
    def __init__(self, url = "https://livestream.com/nrl/"):
        response = urllib2.urlopen(url)
        html = response.read()
        #with open ("/home/older/projects/plugin.video.nrl/notinrepo/index.html", "r") as myfile:
        #  html=myfile.read()
        
        script_re = re.compile("<script.*>\s*window.config\s*=\s*(.*);\s*<\/script>", re.MULTILINE)
        match = script_re.search(html)
        window_config = json.loads(match.group(1))
        account = json.loads(window_config["account"])
        past_events = list()
        for event in account["past_events"]["data"]:
            current_event = Event()
            current_event.img_src = event["logo"]["url"]
            current_event.full_name = event["full_name"]
            current_event.date_text = event["end_time"]
            current_event.href = '/{}/events/{}'.format(event["owner"]["short_name"], event["id"])
            past_events.append(current_event)

        self.event_lists = dict()
        self.event_lists[Event.status_past] = past_events
        
    def event_lists(self):
        return self.event_lists

